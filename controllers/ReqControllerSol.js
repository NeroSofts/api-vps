const db = require('../config/db');
module.exports = {
    async insert(req, res){
        let client = req.params.client;
        try {
            let response = await db.query(`SELECT * FROM clients WHERE id = ${client}`)
            
            let data = {
                client: response[0][0].id,
                id_route: response[0][0].id_route,
                latitude: response[0][0].latitude,
                longitude: response[0][0].longitude,
                title: response[0][0].address,
                data_ins: new Date()
            }
            let response_ins = await db.query(`INSERT INTO requests SET ?`, [data])
            res.json(response_ins[0]);
        } catch (error) {
            console.log(error)
        }
    },
    async findEnd(req, res){
        let route = req.params.route;
        try {
            let response = await db.query(`SELECT * FROM requests WHERE status = 0 AND id_route = ${route} ORDER BY id DESC LIMIT 1`)
            if(response[0] == ''){
                res.json([])
            }else{
                res.json(response[0][0]);
            }
        } catch (error) {
            console.log(error)
        }
    },
    async acceptSol(req, res){
        let id_sol = req.params.id_sol;
        let id_wm = req.params.id_wm;
        try {
            let response = await db.query(`UPDATE requests SET status = 1, id_watchman = ? WHERE id = ?`, [id_wm, id_sol])
            res.json(response[0]);
        } catch (error) {
            console.log(error)
        }
    },
    async updateLocation(req, res){
        let id_sol = req.params.id_sol;
        let name = req.params.name;
        try {
            let response = await db.query(`UPDATE requests SET firebase_loc = ? WHERE id = ?`, [name, id_sol])
            res.json(response[0]);
        } catch (error) {
            console.log(error)
        }
    },
    async detailsSol(req, res){
        let id = req.params.id;
        try {
            ret = []
            let response = await db.query(`SELECT c.*, r.* FROM requests r JOIN clients c ON c.id = r.client AND r.id = ?`, [id])
            ret[0] = response[0][0];

            if(response[0][0].id_watchman != 0){
                let responsew = await db.query(`SELECT * FROM watchman WHERE id = ?`, [response[0][0].id_watchman])
                ret[0]['vigilante'] =  responsew[0][0].name;
                ret[0]['osIDV'] =  responsew[0][0].osID;
            }

            res.json(ret[0]);
        } catch (error) {
            console.log(error)
        }
    },
    async findSolByWatchmanId(req, res){
        let id_wm = req.params.id_wm;
        try {
            let response = await db.query(`SELECT r.*, c.name FROM requests r JOIN clients c ON c.id = r.client AND r.id_watchman = ? ORDER BY r.id DESC`, [id_wm])
            res.json(response[0]);
        } catch (error) {
            console.log(error)
        }
    },
    async findSolByClientId(req, res){
        let id_cli = req.params.id_cli;
        try {
            let response = await db.query(`SELECT r.*, c.name, w.name as vigilante FROM requests r JOIN clients c JOIN watchman w ON w.id = r.id_watchman AND c.id = r.client AND r.client = ? ORDER BY r.id DESC`, [id_cli])
            res.json(response[0]);
        } catch (error) {
            console.log(error)
        }
    }
}