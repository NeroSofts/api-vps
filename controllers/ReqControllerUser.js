const db = require('../config/db');

module.exports = {
    async auth(req, res){
        let email       = req.body.email;
        let password    = req.body.password;
        try {
            let response = await db.query(`SELECT id FROM users WHERE email = ? AND password = ?`, [email, password])
            if(response[0] == ''){
                res.json({ error: 'emailorpassword' });
            }else{
                res.json(response[0][0]);
            }
        } catch (error) {
            console.log(error)
        }
    },
    async insert(req, res){
        let datas = {
            "name":         req.body.name,
            "email":        req.body.email,
            "password":     req.body.password,
            "status":       1
          }

        try {
            let user = await db.query('SELECT * FROM users WHERE email = ?', [req.body.email]);
            if(user[0].length > 0){
                res.json({ error: 'emailexists' });
            }else{
                try {
                    let response = await db.query(`INSERT INTO users SET ?`, [datas])
                    res.json(response[0]);
                } catch (error) {
                    console.log(error);
                    res.json(error);
                }
            }
            
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async update(req, res){
        let id = req.params.id;
        let datas = {};
        if(req.body.password == ''){
            datas = {
                "name":         req.body.name,
                "email":        req.body.email
            }
        }else{
            datas = {
                "name":         req.body.name,
                "email":        req.body.email,
                "password":     req.body.password
            }
        }
        

        try {

            //SELECIONA EMAIL ATUAL
            let [actualEmail] = await db.query('SELECT * FROM users WHERE id = ?', [id]);

            let user = await db.query('SELECT * FROM users WHERE email = ?', [req.body.email]);
            if(user[0].length > 0 && actualEmail[0].email != req.body.email){
                res.json({ error: 'emailexists' });
            }else{
                let updateUser = await db.query('UPDATE users SET ? WHERE id = ?', [datas, id])
                res.json(updateUser[0]);
            }
            
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findAll(req, res){
        try {
            let response = await db.query('SELECT * FROM users');
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findById(req, res){
        let id = req.params.id;
        try {
            let response = await db.query('SELECT * FROM users WHERE id = ?', id);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findByKey(req, res){
        let key = req.params.key;

        let sql = `SELECT * FROM users WHERE name like "%${key}%" OR email like "%${key}%"`;

        try {
            let response = await db.query(sql);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async delete(req, res){
        let id = req.params.id;
        try {
            let response = await db.query('DELETE FROM users WHERE id = ?', id);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    }
}