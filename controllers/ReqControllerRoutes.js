const db = require('../config/db');


module.exports = {
    async insert(req, res){
        let datas = {
            "description": req.body.description
          }

        try {
            let response = await db.query(`INSERT INTO routes SET ?`, [datas])
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async update(req, res){
        let id = req.params.id;
        let datas = {
            "description": req.body.description,
        }

        try {

            let updateUser = await db.query('UPDATE routes SET ? WHERE id = ?', [datas, id])
            res.json(updateUser[0]);

        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findAll(req, res){
        try {
            let response = await db.query('SELECT * FROM routes');
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findById(req, res){
        let id = req.params.id;
        try {
            let response = await db.query('SELECT * FROM routes WHERE id = ?', id);
            res.json(response[0][0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findByKey(req, res){
        let key = req.params.key;

        let sql = `SELECT * FROM routes WHERE description like "%${key}%"`;

        try {
            let response = await db.query(sql);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    }

}