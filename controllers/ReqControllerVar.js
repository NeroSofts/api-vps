const db = require('../config/db');

module.exports = {
    async insert(req, res){
        let datas = {
            "descricao": req.body.descricao,
            "id_produto": req.body.id_produto
          }

        try {
            let response = await db.query('INSERT INTO tamanhos SET ?', datas)
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

    async update(req, res){
        let id = req.params.id;
        let datas = {
            "descricao": req.body.descricao
          }

        try {
            let response = await db.query('UPDATE tamanhos SET ? WHERE id = ?', [datas, id])
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

    async find(req, res){
        let id = req.params.id;
        try {
            let sql = 'SELECT * FROM tamanhos WHERE id_produto = ?';
            let response = await db.query(sql, [id]);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
}