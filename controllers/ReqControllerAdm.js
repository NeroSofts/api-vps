const db = require('../config/db');

module.exports = {
    async auth(req, res){
        let email = req.body.email;
        let senha = req.body.senha;
        try {
            let response = await db.query('SELECT * FROM administradores WHERE email = ? AND senha = ?', [email, senha])
            if(response[0] == ''){
                res.json({ response: 'emailorpassword' });
            }else{
                res.json(response[0]);
            }
        } catch (error) {
            console.log(error)
        }
    },
    async insert(req, res){
        let datas = {
            "nome":     req.body.nome,
            "email":    req.body.email,
            "senha":    req.body.senha,
            "status":   req.body.status
          }

        try {
            let user = await db.query('SELECT * FROM administradores WHERE email = ?', [req.body.email]);
            if(user[0].length > 0){
                res.json({ error: 'emailexists' });
            }else{
                try {
                    let response = await db.query(`INSERT INTO administradores SET ?`, [datas])
                    res.json(response[0]);
                } catch (error) {
                    console.log(error);
                    res.json(error);
                }
            }
            
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async update(req, res){
        let id = req.params.id;
        let datas = {
            "nome":     req.body.nome,
            "email":    req.body.email,
            "senha":    req.body.senha,
            "status":   req.body.status
        }

        try {

            //SELECIONA EMAIL ATUAL
            let [actualEmail] = await db.query('SELECT * FROM administradores WHERE id = ?', [id]);

            let user = await db.query('SELECT * FROM administradores WHERE email = ?', [req.body.email]);
            if(user[0].length > 0 && actualEmail[0].email != req.body.email){
                res.json({ response: 'emailexists' });
            }else{
                let updateUser = await db.query('UPDATE administradores SET ? WHERE id = ?', [datas, id])
                res.json(updateUser[0]);
            }
            
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findAll(req, res){
        try {
            let response = await db.query('SELECT * FROM administradores');
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findById(req, res){
        let id = req.params.id;
        try {
            let response = await db.query('SELECT * FROM administradores WHERE id = ?', id);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findByKey(req, res){
        let key = req.params.key;

        let sql = 'SELECT * FROM administradores WHERE nome like "%'+key+'%" OR email like "%'+key+'%"';

        try {
            let response = await db.query(sql);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
}