const db = require('../config/db');

module.exports = {
    async insert(req, res){
        let datas = {
            "nome": req.body.nome,
            "descricao": req.body.descricao,
            "id_fabricante": req.body.id_fabricante,
            "id_categoria": req.body.id_categoria,
            "valor": req.body.valor,
            "tamanho": req.body.tamanho,
          }

        try {
            let response = await db.query('INSERT INTO produtos SET ?', [datas])
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

    async update(req, res){
        let id = req.params.id;
        let datas = {
            "nome": req.body.nome,
            "descricao": req.body.descricao,
            "id_fabricante": req.body.id_fabricante,
            "id_categoria": req.body.id_categoria,
            "valor": req.body.valor,
            "tamanho": req.body.tamanho,
          }

        try {
            let response = await db.query('UPDATE produtos SET ? WHERE id = ?', [datas, id])
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

    async findAll(req, res){
        let page = req.params.page;
        let offset = (page-1) * 10;
        console.log(offset);
        if(page == 99){
            try {
                let sql = `SELECT p.*, f.nome fabricante, IF(tamanho = 0, p.quantidade, 0) estoque, (SELECT SUM(t.quantidade) estoque FROM tamanhos t WHERE t.id_produto = p.id) estoque_tamanho, c.nome categoria FROM produtos p JOIN fabricantes f JOIN categorias c ON f.id = p.id_fabricante AND c.id = p.id_categoria`;
            let response = await db.query(sql);
                res.json(response[0]);
            } catch (error) {
                console.log(error);
                res.json(error);
            }
        }else{
            try {
                let sql = `SELECT p.*, f.nome fabricante, IF(tamanho = 0, p.quantidade, 0) estoque, (SELECT SUM(t.quantidade) estoque FROM tamanhos t WHERE t.id_produto = p.id) estoque_tamanho, c.nome categoria FROM produtos p JOIN fabricantes f JOIN categorias c ON f.id = p.id_fabricante AND c.id = p.id_categoria LIMIT ${offset}, 10`;
            let response = await db.query(sql);
                res.json(response[0]);
            } catch (error) {
                console.log(error);
                res.json(error);
            }
        }
        
    },
    async findLatest(req, res){
        try {
            let sql = `SELECT p.*, f.nome fabricante, IF(tamanho = 0, p.quantidade, 0) estoque, (SELECT SUM(t.quantidade) estoque FROM tamanhos t WHERE t.id_produto = p.id) estoque_tamanho, c.nome categoria FROM produtos p JOIN fabricantes f JOIN categorias c ON f.id = p.id_fabricante AND c.id = p.id_categoria`;
            let response = await db.query(sql);
            var data_return = [];
            for (let i = 0; i < response[0].length; i++) {
                let valor = response[0][i]['valor'];
                valor = valor.toFixed(2);
                valor = valor.toString().replace('.', ',');
                data_return[i] = {
                    id: response[0][i]['id'],
                    nome: response[0][i]['nome'],
                    descricao: response[0][i]['descricao'],
                    valor: valor,
                    tamanho: response[0][i]['tamanho'],
                    id_fabricante: response[0][i]['id_fabricante'],
                    fabricante: response[0][i]['fabricante'],
                }

                let sql_fotos = `SELECT * FROM fotos WHERE id_produto = ${response[0][i]['id']} AND foto_principal = 1`;
                let response_fotos = await db.query(sql_fotos);
                if(response_fotos[0].length > 0){
                    data_return[i]['foto_principal'] = 'https://nerohost.com.br/fotos_gil/fotos/'+response_fotos[0][0]['foto_dir'];
                }else{

                    data_return[i]['foto_principal'] = 'https://www.ferramentastenace.com.br/wp-content/uploads/2017/11/sem-foto.jpg';
                    

                    
                }

            }

            res.json(data_return);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

    async count(req, res){
        try {
            let sql = 'SELECT p.*, f.nome fabricante, IF(tamanho = 0, p.quantidade, 0) estoque, (SELECT SUM(t.quantidade) estoque FROM tamanhos t WHERE t.id_produto = p.id) estoque_tamanho, c.nome categoria FROM produtos p JOIN fabricantes f JOIN categorias c ON f.id = p.id_fabricante AND c.id = p.id_categoria';
            let response = await db.query(sql);
            res.json( Math.ceil((response[0].length) / 10) );
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

    async findByKeyDash(req, res){
        let key = req.params.key;

        try {
            let sql = 'SELECT p.*, f.nome fabricante, IF(tamanho = 0, p.quantidade, 0) estoque, (SELECT SUM(t.quantidade) estoque FROM tamanhos t WHERE t.id_produto = p.id) estoque_tamanho, c.nome categoria FROM produtos p JOIN fabricantes f JOIN categorias c ON f.id = p.id_fabricante AND c.id = p.id_categoria AND (f.nome like "%'+key+'%" or p.nome like "%'+key+'%" or p.id like "%'+key+'%" or f.nome like "%'+key+'%" or c.nome like "%'+key+'%") ';
        let response = await db.query(sql);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

    async findByKey(req, res){
        let key = req.body.key;
        let body = req.body;
        let add = '';

        if(body.categoria != 0){
            add = ' AND p.id_categoria = '+body.categoria;
        }else{
            add = '';
        }
        try {
            let sql = 'SELECT p.*, f.nome fabricante, IF(tamanho = 0, p.quantidade, 0) estoque, (SELECT SUM(t.quantidade) estoque FROM tamanhos t WHERE t.id_produto = p.id) estoque_tamanho, c.nome categoria FROM produtos p JOIN fabricantes f JOIN categorias c ON f.id = p.id_fabricante AND c.id = p.id_categoria AND (f.nome like "%'+key+'%" or p.nome like "%'+key+'%" or p.id like "%'+key+'%" or f.nome like "%'+key+'%") '+add;
            let response = await db.query(sql);
            var data_return = [];
            for (let i = 0; i < response[0].length; i++) {
                let valor = response[0][i]['valor'];
                valor = valor.toFixed(2);
                valor = valor.toString().replace('.', ',');
                data_return[i] = {
                    id: response[0][i]['id'],
                    nome: response[0][i]['nome'],
                    descricao: response[0][i]['descricao'],
                    valor: valor,
                    tamanho: response[0][i]['tamanho'],
                    id_fabricante: response[0][i]['id_fabricante'],
                    fabricante: response[0][i]['fabricante'],
                }

                let sql_fotos = `SELECT * FROM fotos WHERE id_produto = ${response[0][i]['id']} AND foto_principal = 1`;
                let response_fotos = await db.query(sql_fotos);
                if(response_fotos[0].length > 0){
                    data_return[i]['foto_principal'] = 'https://nerohost.com.br/fotos_gil/fotos/'+response_fotos[0][0]['foto_dir'];
                }else{

                    data_return[i]['foto_principal'] = 'https://www.ferramentastenace.com.br/wp-content/uploads/2017/11/sem-foto.jpg';
                    

                    
                }

            }
            res.json(data_return);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

}