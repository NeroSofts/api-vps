const db = require('../config/db');


module.exports = {
    async insert(req, res){
        let datas = {
            "id_route":     req.body.id_route,
            "street_name":  req.body.street_name,
            "whole_street": req.body.whole_street
          }

        try {
            let response = await db.query(`INSERT INTO routes_street SET ?`, [datas])
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async update(req, res){
        let id = req.params.id;
        let datas = {
            "street_name":  req.body.street_name,
            "whole_street": req.body.whole_street
        }

        try {

            let updateUser = await db.query('UPDATE routes_street SET ? WHERE id = ?', [datas, id])
            res.json(updateUser[0]);

        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findAll(req, res){
        try {
            let response = await db.query('SELECT * FROM routes_street');
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findById(req, res){
        let id = req.params.id;
        try {
            let response = await db.query('SELECT * FROM routes_street WHERE id = ?', id);
            res.json(response[0][0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findByRouteId(req, res){
        let id = req.params.id;
        try {
            let response = await db.query('SELECT * FROM routes_street WHERE id_route = ?', id);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findByKey(req, res){
        let key = req.params.key;

        let sql = `SELECT * FROM routes_street WHERE street_name like "%${key}%"`;

        try {
            let response = await db.query(sql);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async delete(req, res){
        let id = req.params.id;


        try {
            let response = await db.query('DELETE FROM routes_street WHERE id = ?', id);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    }

}