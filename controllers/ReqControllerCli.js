const db = require('../config/db');

module.exports = {
    async auth(req, res){
        let email       = req.body.email;
        let password    = req.body.password;
        try {
            let response = await db.query(`SELECT * FROM clients WHERE email = ? AND password = ?`, [email, password])
            if(response[0] == ''){
                res.json({ error: 'emailorpassword' });
            }else{
                res.json(response[0][0]);
            }
        } catch (error) {
            console.log(error)
        }
    },
    async insert(req, res){
        let datas = {
            "name":         req.body.name,
            "email":        req.body.email,
            "password":     req.body.password,
            "phone":        req.body.phone,
            "address":      req.body.address,
            "latitude":     req.body.latitude.toString().replace(',', '.'),
            "longitude":    req.body.longitude.toString().replace(',', '.'),
            "cpf":          req.body.cpf,
            "rg":           req.body.rg,
            "id_route":     req.body.id_route
          }


        try {
            let user = await db.query('SELECT * FROM clients WHERE email = ?', [req.body.email]);
            if(user[0].length > 0){
                res.json({ error: 'emailexists' });
            }else{
                try {
                    let response = await db.query(`INSERT INTO clients SET ?`, [datas])
                    res.json(response[0]);
                } catch (error) {
                    console.log(error);
                    res.json(error);
                }
            }
            
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async update(req, res){
        let id = req.params.id;
        let datas = {};
        if(req.body.password == ''){
            datas = {
                "name":         req.body.name,
                "email":        req.body.email,
                "phone":        req.body.phone,
                "address":      req.body.address,
                "latitude":     req.body.latitude.toString().replace(',', '.'),
                "longitude":    req.body.longitude.toString().replace(',', '.'),
                "cpf":          req.body.cpf,
                "rg":           req.body.rg,
                "id_route":     req.body.id_route
              }
        }else{
            datas = {
                "name":         req.body.name,
                "email":        req.body.email,
                "password":     req.body.password,
                "phone":        req.body.phone,
                "address":      req.body.address,
                "latitude":     req.body.latitude.toString().replace(',', '.'),
                "longitude":    req.body.longitude.toString().replace(',', '.'),
                "cpf":          req.body.cpf,
                "rg":           req.body.rg,
                "id_route":     req.body.id_route
              }
        }
        

        try {

            //SELECIONA EMAIL ATUAL
            let [actualEmail] = await db.query('SELECT * FROM clients WHERE id = ?', [id]);

            let user = await db.query('SELECT * FROM clients WHERE email = ?', [req.body.email]);
            if(user[0].length > 0 && actualEmail[0].email != req.body.email){
                res.json({ error: 'emailexists' });
            }else{
                let updateUser = await db.query('UPDATE clients SET ? WHERE id = ?', [datas, id])
                res.json(updateUser[0]);
            }
            
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async updateOS(req, res){
        let id = req.params.id;
        let datas = {
            osID: req.body.osID
        };
        
        try {
            let updateUser = await db.query('UPDATE clients SET ? WHERE id = ?', [datas, id])
            res.json(updateUser[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async updateStatus(req, res){
        let id = req.params.id;
        let datas = {
            status: req.body.status
        };
        
        try {
            let updateUser = await db.query('UPDATE clients SET ? WHERE id = ?', [datas, id])
            res.json(updateUser[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findAll(req, res){
        try {
            let response = await db.query('SELECT c.*, r.description route FROM clients c JOIN routes r ON r.id = c.id_route');
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async findById(req, res){
        let id = req.params.id;
        try {
            let response = await db.query('SELECT c.*, r.description route FROM clients c JOIN routes r ON r.id = c.id_route AND c.id = ?', id);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findByKey(req, res){
        let key = req.params.key;

        let sql = `SELECT c.*, r.description route FROM clients c JOIN routes r ON r.id = c.id_route AND (c.name like "%${key}%" OR c.email like "%${key}%" OR r.description like "%${key}%")`;

        try {
            let response = await db.query(sql);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async delete(req, res){
        let id = req.params.id;
        try {
            let response = await db.query('DELETE FROM clients WHERE id = ?', id);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    }
}