const db = require('../config/db');

module.exports = {
    async insert(req, res){
        let type = req.params.type;
        let tipo_ins = "E";
        if(type == 0){
            tipo_ins = "S";
        }
        let data_product = {
            "descricao": req.body.descricao,
            "id_produto": req.body.id_produto,
            "id_tamanho": req.body.id_tamanho,
            "quantidade": req.body.quantidade,
            "tipo": tipo_ins
          }

        
            if(data_product.id_tamanho == 0){
                //INSERE QUANTIDADE DIRETO NO PRODUTO
                try {
                    let response_product = await db.query('SELECT * FROM produtos WHERE id = ?', [data_product.id_produto]);
                    let qnt_actual_product = response_product[0][0].quantidade;
                    if(type == 0){
                        if(qnt_actual_product == 0){
                            res.json({'error':'Produto com quantidade igual a zero!'});
                            return false;
                        }

                        if((qnt_actual_product - data_product.quantidade) <= 0){
                            res.json({'error':'A quantidade que você esta tentando retirar é menor do que a quantidade atual!'});
                            return false;
                        }

                        var qnt_new_product = parseInt(qnt_actual_product) - parseInt(data_product.quantidade);
                    }else{
                        var qnt_new_product = parseInt(qnt_actual_product) + parseInt(data_product.quantidade);
                    }
                } catch (error) {
                    console.log(error);
                    return false;
                }

                try {
                    //INSERE
                    let response_product_ins = await db.query(`UPDATE produtos SET quantidade = ${qnt_new_product} WHERE id = ${data_product.id_produto}`);
                } catch (error) {
                    console.log(error);
                    return false;
                }


            }else{
                //INSERE QUANTIDADE NO TAMANHO ESCOLHIDO
                try {
                    let response_variation = await db.query('SELECT * FROM tamanhos WHERE id = ?', [data_product.id_tamanho]);
                    let qnt_actual_var = response_variation[0][0].quantidade;
                    if(type == 0){
                        if(qnt_actual_var == 0){
                            res.json({'error':'Variação com quantidade igual a zero!'});
                            return false;
                        }

                        if((qnt_actual_var - data_product.quantidade) <= 0){
                            res.json({'error':'A quantidade que você esta tentando retirar é menor do que a quantidade atual!'});
                            return false;
                        }

                        var qnt_new_var = parseInt(qnt_actual_var) - parseInt(data_product.quantidade);
                    }else{
                        var qnt_new_var = parseInt(qnt_actual_var) + parseInt(data_product.quantidade);
                    }
                } catch (error) {
                    console.log(error);
                    return false;
                }

                try {
                    //INSERE
                    let response_product_ins = await db.query(`UPDATE tamanhos SET quantidade = ${qnt_new_var} WHERE id = ${data_product.id_tamanho}`);
                } catch (error) {
                    console.log(error);
                    return false;
                }
            }

            try {
                let response = await db.query('INSERT INTO estoque SET ?', [data_product])
                res.json(response[0]);
            } catch (error) {
                console.log(error);
            }
            
    },

    async find(req, res){
        let id_produto = req.params.product;
        try {
            let sql = `SELECT * FROM estoque WHERE id_produto = ${id_produto}`;
            let response = await db.query(sql);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
}