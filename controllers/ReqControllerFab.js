const db = require('../config/db');

module.exports = {
    async insert(req, res){
        let datas = {
            nome: req.body.nome
        }

        try {
            let response = await db.query('INSERT INTO fabricantes SET ?', [datas])
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }

    },
    async update(req, res){
        let id = req.params.id;
        let datas = {
            nome: req.body.nome
        }

        try {
            let response = await db.query('UPDATE fabricantes SET ? WHERE id = ?', [datas, id])
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }

    },
    async findAll(req, res){
        try {
            let response = await db.query('SELECT * FROM fabricantes');
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findById(req, res){
        let id = req.params.id;
        try {
            let response = await db.query('SELECT * FROM fabricantes WHERE id = ?', id);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },
    async findByName(req, res){
        let name = req.params.name;
        try {
            let response = await db.query('SELECT * FROM fabricantes WHERE nome like "%'+name+'%"');
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    }
}