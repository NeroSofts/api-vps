const db = require('../config/db');

module.exports = {

    async insert(req, res){
        let datas = {
            "nome": req.body.nome
          }

        try {
            let response = await db.query('INSERT INTO categorias SET ?', datas)
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

    async update(req, res){
        let id = req.params.id;
        let datas = {
            "nome": req.body.nome
          }

        try {
            let response = await db.query('UPDATE categorias SET ? WHERE id = ?', [datas, id])
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
    },

    async findAll(req, res){
        try {
            let sql = `SELECT * FROM categorias`;
        let response = await db.query(sql);
            res.json(response[0]);
        } catch (error) {
            console.log(error);
            res.json(error);
        }
        
    },

}