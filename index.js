const express = require('express');
const app = express();

app.use(express.json());



var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', '*');
    next();
}


//USE
app.use(allowCrossDomain);

const ControllerUsers       = require('./controllers/ReqControllerUser');
const ControllerSol         = require('./controllers/ReqControllerSol');
const ControllerRoutes      = require('./controllers/ReqControllerRoutes');
const ControllerWatchMan    = require('./controllers/ReqControllerWatchMan');
const ControllerStreet      = require('./controllers/ReqControllerStreet');
const ControllerCli         = require('./controllers/ReqControllerCli');

// ROUTES


//USERS
app.post('/user/login',                 ControllerUsers.auth);
app.post('/user/insert',                ControllerUsers.insert);
app.put('/user/update/:id',             ControllerUsers.update);
app.get('/user/findAll',                ControllerUsers.findAll);
app.get('/user/findById/:id',           ControllerUsers.findById);
app.get('/user/findByKey/:key',         ControllerUsers.findByKey);
app.delete('/user/delete/:id',          ControllerUsers.delete);

//ROTAS
app.post('/route/insert',               ControllerRoutes.insert);
app.put('/route/update/:id',            ControllerRoutes.update);
app.get('/route/findAll',               ControllerRoutes.findAll);
app.get('/route/findById/:id',          ControllerRoutes.findById);
app.get('/route/findByKey/:key',        ControllerRoutes.findByKey);

//RUAS
app.post('/street/insert',              ControllerStreet.insert);
app.put('/street/update/:id',           ControllerStreet.update);
app.get('/street/findAll',              ControllerStreet.findAll);
app.get('/street/findById/:id',         ControllerStreet.findById);
app.get('/street/findByRouteId/:id',    ControllerStreet.findByRouteId);
app.get('/street/findByKey/:key',       ControllerStreet.findByKey);
app.delete('/street/delete/:id',        ControllerStreet.delete);


//VIGILANTES
app.post('/watchman/login',             ControllerWatchMan.auth);
app.post('/watchman/insert',            ControllerWatchMan.insert);
app.put('/watchman/update/:id',         ControllerWatchMan.update);
app.put('/watchman/updateos/:id',       ControllerWatchMan.updateOS);
app.get('/watchman/findAll',            ControllerWatchMan.findAll);
app.get('/watchman/findById/:id',       ControllerWatchMan.findById);
app.get('/watchman/findByKey/:key',     ControllerWatchMan.findByKey);
app.delete('/watchman/delete/:id',      ControllerWatchMan.delete);

//CLIENTES
app.post('/client/login',               ControllerCli.auth);
app.post('/client/insert',              ControllerCli.insert);
app.put('/client/update/:id',           ControllerCli.update);
app.put('/client/updateos/:id',         ControllerCli.updateOS);
app.put('/client/updatestatus/:id',     ControllerCli.updateStatus);
app.get('/client/findAll',              ControllerCli.findAll);
app.get('/client/findById/:id',         ControllerCli.findById);
app.get('/client/findByKey/:key',       ControllerCli.findByKey);
app.delete('/client/delete/:id',        ControllerCli.delete);


//RONDAS
app.get('/sol/insert/:client',          ControllerSol.insert);
app.get('/sol/findEnd/:route',          ControllerSol.findEnd);
app.get('/sol/accept/:id_sol/:id_wm',   ControllerSol.acceptSol);
app.get('/sol/location/:id_sol/:name',  ControllerSol.updateLocation);
app.get('/sol/details/:id',             ControllerSol.detailsSol)
app.get('/sol/:id_wm',                  ControllerSol.findSolByWatchmanId)
app.get('/sol/cli/:id_cli',             ControllerSol.findSolByClientId)




//END
const PORT = process.env.PORT || 8089;
app.listen(PORT, ()=>{
    console.log('rodando na porta '+PORT);
});
